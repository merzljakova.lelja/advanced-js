// Обьясните своими словами, как вы понимаете асинхронность в Javascript
// Благодаря асинхронности можно выполнять долгие сетевые запросы без перезагрузки страницы и блокирования основного потока.

const getUserIp = "https://api.ipify.org/?format=json";
const buttonGetIp = document.getElementById("check-Ip");
buttonGetIp.addEventListener("click", getIP);
    async function getIP() {
      const userIp = await fetch(getUserIp)
      .then((response) => response.json());
      const location = await fetch(`http://ip-api.com/json/${userIp.ip}`)
      .then((response) => response.json());
      const ip = new findIP(location)
      ip.render()
      buttonGetIp.removeEventListener("click", getIP)
    }
  
  class findIP{
      constructor({timezone, country, regionName, city, region}){
          this.timezone = timezone;
          this.country = country;
          this.regionName = regionName;
          this.city = city;
          this.region = region;
      }
       render() {
    const root = document.querySelector("#root")
    root.insertAdjacentHTML(
        "beforeend",
        `<ul>INFO:<ul>
        <li>Continent: ${this.timezone}</li>
        <li>Country: ${this.country}</li>
        <li>Region: ${this.regionName}</li>
        <li>City: ${this.city}</li>
        <li>District: ${this.region}</li>`
    )    
  }
  }