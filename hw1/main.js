class Employee{
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
      }
      get name(){
          return this._name;
      };
      set name(value){
          this._name = value;
      };
    
      get age(){
          return this._age;
      };
      set age(value){
        this._age = value
      };
      get salary(){
        return this._salary;
    };
    set salary(value){
        this._salary = value;
    };
    }

    class Progremmer extends Employee{
        constructor(name, age, salary,lang){
            super(name, age, salary)
            this.lang = lang;
        }
        get salary(){
            return super.salary * 3;
        };
        set salary(value){
            super.salary = value;
        };
    }
    
    const progremmer1 = new Progremmer("alona",32,1200,"en");
    const progremmer2 = new Progremmer("fibi",3,200,"may");
    const progremmer3 = new Progremmer("alfred",31,5000,"uk");
    console.log(progremmer1);
    console.log(progremmer2);
    console.log(progremmer3);