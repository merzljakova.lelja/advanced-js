const USERS_URL = "https://ajax.test-danit.com/api/json/users";
const POSTS = "https://ajax.test-danit.com/api/json/posts";
const urls = [USERS_URL,POSTS]
async function fetchAll(){
    const res = await Promise.all(urls.map(u => fetch(u)))
    return await Promise.all(res.map(r => r.json()))
}

 fetchAll().then((data) => {
    let [USERS, POSTS] = data;  
   let users = USERS.map(user => {
    return  { id, name, email } = user;      
    });
    const filtered = users.map(i => {
        let arr = []
        const id = i.id
        POSTS.map(index => {
            if (index.userId === id) {
                index.name = i.name
                index.email = i.email
                arr.push(index)
            }
            const card = new Card(index)
                card.render()
        });
        return arr;
    })
})

class Card {
    constructor({name, email, title, body, id}) {
        this.name = name
        this.email = email
        this.title = title
        this.body = body
        this.id = id
    }

    render() {
        let wrapper = document.createElement("div")
        wrapper.dataset.id = this.id
        wrapper.addEventListener("click", this.cardRemove)
        wrapper.classList.add("card")
        wrapper.insertAdjacentHTML(
            "afterbegin",
            ` <div class="card__user">
                <img src="145812.png" alt="">
                <a href="#" class="card__user--name">${this.name}</a>
                <a href="mailto:${this.email}">${this.email}</a>
            </div>
            <div class="headline-wrapper">
                <h2 class="card__headline">${this.title}</h2>
                <button class="close-btn">&#x2715</button>
            </div>
            <div class="card__body">
                <p>${this.body}</p>
            </div>
        `
        );
         const root = document.getElementById("root")
        root.append(wrapper)
    }   
    cardRemove(e) {
        if (e.target.classList.contains("close-btn")) {
            const id = e.target.closest("[data-id]").dataset.id
            this.remove()
            fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
                method: "DELETE"
            })
        }
    }
}

