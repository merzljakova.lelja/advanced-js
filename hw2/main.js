// Теоретический вопрос
// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
// Эту конструкцию уместно использовать с данными от пользователя или с данными с сервера

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

const root = document.getElementById("root");

function createList(){
    
    books.forEach((element,i) => {
         try {
              if (!element.author) { 
                throw new Error(`Данные некорректны, в обьекте массива под индексом ${i} должно быть свойство author`);
              };
              if(!element.name){
                throw new Error(`Данные некорректны, в обьекте массива под индексом ${i} должно быть свойство name`);
              };
              if(!element.price){
                throw new Error(`Данные некорректны, в обьекте массива под индексом ${i} должно быть свойство price`);
              }
              render(element)
            
              } catch (e) {
                  console.error(e.name + ': ' + e.message);
              }
    })
               }
               
function render(element){
  const list = document.createElement("ul");
  if (element.name && element.author && element.price){
    const Item = document.createElement("li");
    Item.innerHTML = `author: ${element.author};<br> name: ${element.name};<br> price: ${element.price}`;
    list.append(Item);
      root.append(list);
  }
}

createList(books);
