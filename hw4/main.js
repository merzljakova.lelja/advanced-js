"use strict";
// ## Теоретический вопрос
// Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
// Tехнология, позволяющая взаимодействовать с сервером без перезагрузки страницы. 
// Например, получать или передавать какие-нибудь данные.

const URL = `https://ajax.test-danit.com/api/swapi/films`;
const root = document.getElementById("root");

class FilmAndInformationAbout{
  constructor(url,root){
    this.url = url;
    this.root = root;
  }
getPlanetsAndCharacters(){
  fetch(this.url)
  .then((response) => response.json())
  .then((data) => {      
   data.forEach(({name,episodeId,openingCrawl,characters}) => {
     const filmItem = document.createElement("div")
       filmItem.innerHTML = `<h2>Film: ${name}</h2>
                          <p>Episode №${episodeId}</p>
                             <p>${openingCrawl}</p>`;
        this.root.append(filmItem);
    return Promise.all(characters.map((u) => 
    fetch(u)
     .then(response => response.json())
     .then(data => {
      const charactersList = document.createElement("ul");
              charactersList.innerHTML = `<li>${data.name}</li>`;
               filmItem.append(charactersList)
     })
     .catch(err => console.log(err))))
    })   
     })
}
}

const filmAndInfo = new FilmAndInformationAbout(URL,root);
filmAndInfo.getPlanetsAndCharacters()




